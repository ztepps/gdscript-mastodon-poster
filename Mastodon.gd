tool
extends EditorScript

class Mastodon:
	const VERSION = "0.0.1"
	const WEBSITE = "https://gitlab.com/ztepps/gdscript-mastodon-poster"
	
	enum Visibility {
		PUBLIC,
		UNLISTED,
		PRIVATE,
		DM
	}
	
	var USER_AGENT = "Godot " + Engine.get_version_info()["string"] + " / " + VERSION
	var _instance
	var _tokens = {
		"access": "",
		"secret": "",
		"client": ""
	}
	var scopes
	var redirect_uris
	
	var _client
	var _base_headers = [
		USER_AGENT,
	]
	
	func _init(instance):
		_instance = instance
		
		_client = HTTPClient.new()
		var err = _client.connect_to_host(_instance, 443, true)
		
		if err == OK:
			while _client.get_status() == HTTPClient.STATUS_CONNECTING or _client.get_status() == HTTPClient.STATUS_RESOLVING:
				_client.poll()
				OS.delay_usec(500)
	
	func register_app(name, redirect_uris = "urn:ietf:wg:oauth:2.0:oob", scopes = "read write", website = null):
		if not website:
			website = WEBSITE
		
		_perform_request(HTTPClient.METHOD_POST,
												"apps",
												{ "client_name": name, "redirect_uris": redirect_uris, "scopes": scopes, "website": website })
		
		if _client.has_response():
			var resp = JSON.parse(_read_client_response())
			
			if resp.error == OK and not 'error' in resp.result:
				_tokens["client"] = resp.result["client_id"]
				_tokens["secret"] = resp.result["client_secret"]
			else:
				return false
		else:
			return false
		
		self.redirect_uris = redirect_uris
		self.scopes = scopes
		
		return true
	
	func login(oauth = true, email = "", password = ""):
		if _tokens['client'] and _tokens['secret']:
			if oauth:
				OS.shell_open("https://" +
				_instance +
				"/oauth/authorize" +
				_client.query_string_from_dict(
					{ 'scope': scopes,
					  'redirect_uri': redirect_uris,
					  'client_id': _tokens['client'],
					  'respone_type': 'code'
					}))
			else:
				print("logging in")
				_perform_request(HTTPClient.METHOD_POST,
				"oauth/token", 
				{ 'client_id': _tokens['client'],
				  'client_secret': _tokens['secret'],
				  'grant_type': 'password',
				  'username': email,
				  'password': password,
				  'scope': scopes
				})
				
				if _client.get_status() != HTTPClient.RESPONSE_FOUND:
					if _client.has_response():
						var resp = JSON.parse(_read_client_response())
						
						if resp.error == OK:
							_tokens['access'] = resp.result['access_token']
						else:
							return false
					else:
						return false
				else:
					return false
		else:
			return false
		
		return true
	
	func post_status(status, privacy = Visibility.PUBLIC, nsfw = false, cw = ""):
		match privacy:
			PUBLIC:
				privacy = "public"
			UNLISTED:
				privacy = "unlisted"
			PRIVATE:
				privacy = "private"
			DM:
				privacy = "direct"
		
		var post = {}
		
		post['status'] = status
		post['visibility'] = privacy
		
		if cw != "":
			post['spoiler_text'] = cw
		if nsfw:
			post['sensitive'] = str(true).to_lower()
		
		_perform_request(HTTPClient.METHOD_POST, "statuses", post)
		
		# debugging
#		if _client.has_response():
#			var resp = JSON.parse(_read_client_response())
#
#			if resp.error == OK and not 'error' in resp.result:
#				print(resp.result)
#			else:
#				print(str(resp.error) + "\n\n" + str(resp.result))
	
	func _api_path(path):
		if "oauth" in path:
			return "/" + path
		else:
			return "/api/v1/" + path
	
	func _read_client_response():
		var rb = PoolByteArray()
		
		while _client.get_status() == HTTPClient.STATUS_BODY:
			_client.poll()
			var chunk = _client.read_response_body_chunk()
			
			if chunk.size() == 0:
				OS.delay_usec(1000)
			else:
				rb += chunk
		
		return rb.get_string_from_ascii()
	
	func _perform_request(method, path, params = {}):
		path = _api_path(path)
		params = _client.query_string_from_dict(params)
		
		var headers = _base_headers.duplicate()
		if _tokens['access'] != '':
			headers.append("Authorization: Bearer " + _tokens['access'])
		
		match method:
			HTTPClient.METHOD_GET:
				
				pass
			HTTPClient.METHOD_POST:
				headers.append("Content-Type: application/x-www-form-urlencoded")
				headers.append("Content-Length: " + str(params.length()))
				_client.request(method, path, headers, params)
				pass
		
		while _client.get_status() == HTTPClient.STATUS_REQUESTING:
			_client.poll()
			OS.delay_usec(500)


func _run():
	var masto = Mastodon.new("YOUR_INSTANCE")
	masto.register_app("GDScript Test")
	masto.login(false, "YOUR_EMAIL", "YOUR_PASS")
	masto.post_status("posting from godot", Mastodon.Visibility.PRIVATE, false, "testing")
